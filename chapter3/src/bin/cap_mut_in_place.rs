fn cap_mut_in_place(max: i32, v: &mut Vec<i32>) {
    for index in 0..v.len() {
        if v[index] > max {
            v[index] = max;
        }
    }
}

fn main() {
    let mut values = vec![1, 2, 3, 100, 5];
    cap_mut_in_place(10, &mut values);

    for value in values {
        println!("{}", value);
    }
}
