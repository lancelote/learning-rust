struct Point {
    x: i32,
    y: i32,
}

fn add_points(a: Point, b: Point) -> Point {
    Point {
        x: a.x + b.x,
        y: a.y + b.y,
    }
}

fn main() {
    let p1 = Point { x: 1, y: 2 };
    let p2 = Point { x: 3, y: 4 };
    let p3 = add_points(p1, p2);

    println!("Point({}, {})", p3.x, p3.y);
}
