enum Action {
    Drive,
    Turn(Direction),
    Pickup,
    Stop,
}

enum Direction {
    Left,
    Right,
}

fn print_action(action: Action) {
    match action {
        Action::Drive => println!("drive forward"),
        Action::Turn(direction) => match direction {
            Direction::Left => println!("turn left"),
            Direction::Right => println!("turn right"),
        },
        Action::Pickup => println!("pickup"),
        Action::Stop => println!("stop"),
    }
}

fn main() {
    let program = vec![
        Action::Drive,
        Action::Turn(Direction::Left),
        Action::Drive,
        Action::Pickup,
        Action::Turn(Direction::Left),
        Action::Turn(Direction::Left),
        Action::Turn(Direction::Left),
        Action::Turn(Direction::Left),
        Action::Drive,
        Action::Turn(Direction::Right),
        Action::Drive,
        Action::Stop,
    ];

    for action in program {
        print_action(action);
    }
}
