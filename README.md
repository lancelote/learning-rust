# learning-rust

Code from "Learning Rust" video course by Leo Tindall

## TOC

- [x] Chapter 1: The Power of Rust
- [x] Chapter 2: Rustup and Cargo
- [x] Chapter 3: Ownership and Borrowing
- [x] Chapter 4: Basic Types - Enums and Structs
- [x] Chapter 5: Advanced Types - Traits and Generics
- [x] Chapter 6: Functional Features and Concurrency
- [x] Chapter 7: Idiomatic Rust