extern crate rand;

use rand::random;
use std::io;
use std::io::Write;

fn get_guess() -> u8 {
    loop {
        print!("Input guess: ");
        io::stdout().flush().expect("Could not flush STDOUT");

        let mut guess = String::new();
        io::stdin()
            .read_line(&mut guess)
            .expect("Could not read from STDIN");

        match guess.trim().parse() {
            Ok(v) => return v,
            Err(_) => println!("Not a number"),
        }
    }
}

fn handle_guess(guess: u8, correct: u8) -> bool {
    if guess < correct {
        println!("Too low");
        false
    } else if guess > correct {
        println!("Too high");
        false
    } else {
        println!("Correct!");
        true
    }
}

pub fn main() {
    println!("Welcome to the guessing game!");
    let correct: u8 = random();
    println!("Correct value is: {}", correct);

    loop {
        let guess = get_guess();
        if handle_guess(guess, correct) {
            break;
        }
    }
}
