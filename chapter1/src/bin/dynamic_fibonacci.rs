use std::collections::HashMap;

#[allow(clippy::map_entry, clippy::implicit_hasher)]
pub fn fib_dyn(n: u64, map: &mut HashMap<u64, u64>) -> u64 {
    match n {
        0 | 1 => 1,
        n => {
            if map.contains_key(&n) {
                *map.get(&n).unwrap()
            } else {
                let val = fib_dyn(n - 1, map) + fib_dyn(n - 2, map);
                map.insert(n, val);
                val
            }
        }
    }
}

fn main() {}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn fib_0() {
        let mut map = HashMap::new();
        assert_eq!(fib_dyn(0, &mut map), 1);
    }

    #[test]
    fn fib_1() {
        let mut map = HashMap::new();
        assert_eq!(fib_dyn(1, &mut map), 1);
    }

    #[test]
    fn fib_5() {
        let mut map = HashMap::new();
        assert_eq!(fib_dyn(5, &mut map), 8);
    }
}
