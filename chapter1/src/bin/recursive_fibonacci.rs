const FIB0: u64 = 1;
const FIB1: u64 = 1;

pub fn fib(n: u64) -> u64 {
    if n == 0 {
        FIB0
    } else if n == 1 {
        FIB1
    } else {
        fib(n - 1) + fib(n - 2)
    }
}

fn main() {}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn fib_0() {
        assert_eq!(fib(0), 1);
    }

    #[test]
    fn fib_1() {
        assert_eq!(fib(1), 1);
    }

    #[test]
    fn fib_5() {
        assert_eq!(fib(5), 8);
    }
}
