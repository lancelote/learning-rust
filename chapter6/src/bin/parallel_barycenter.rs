extern crate chapter6;
extern crate itertools;
extern crate rayon;
use chapter6::utils::body::{get_values, Body};
use itertools::Itertools;
use rayon::prelude::*;

fn average(a: f64, b: f64) -> f64 {
    (a + b) / 2.0
}

fn average_with_math(a: f64, b: f64, a_mass: f64, b_mass: f64) -> f64 {
    average(a * a_mass, b * b_mass) / (a_mass + b_mass)
}

fn merge_two_bodies(a: Body, b: Body) -> Body {
    Body {
        x: average_with_math(a.x, b.x, a.mass, b.mass),
        y: average_with_math(a.y, b.y, a.mass, b.mass),
        z: average_with_math(a.z, b.z, a.mass, b.mass),
        mass: a.mass + b.mass,
    }
}

fn merge_all_bodies_recursive(bodies: &[Body]) -> Body {
    println!("bodies: {}", bodies.len());

    if bodies.len() == 1 {
        return bodies[0];
    }

    let tuples: Vec<_> = bodies.iter().tuples().collect();
    let mut merged_bodies: Vec<Body> = tuples
        .into_par_iter() // parallel iterator
        .map(|(a, b)| merge_two_bodies(*a, *b))
        .collect();

    if bodies.len() % 2 != 0 {
        merged_bodies.push(bodies[bodies.len() - 1]);
    }

    merge_all_bodies_recursive(&merged_bodies)
}

fn main() {
    let bodies = get_values();
    let barycenter = merge_all_bodies_recursive(&bodies);
    println!(
        "barycenter: ({}, {}, {})\nmass: {}",
        barycenter.x, barycenter.y, barycenter.z, barycenter.mass
    );
}
