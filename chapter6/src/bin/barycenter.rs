extern crate chapter6;
use chapter6::utils::body::{get_values, Body};

fn average(a: f64, b: f64) -> f64 {
    (a + b) / 2.0
}

fn average_with_math(a: f64, b: f64, a_mass: f64, b_mass: f64) -> f64 {
    average(a * a_mass, b * b_mass) / (a_mass + b_mass)
}

fn merge_two_bodies(a: Body, b: Body) -> Body {
    Body {
        x: average_with_math(a.x, b.x, a.mass, b.mass),
        y: average_with_math(a.y, b.y, a.mass, b.mass),
        z: average_with_math(a.z, b.z, a.mass, b.mass),
        mass: a.mass + b.mass,
    }
}

fn merge_all_bodies_iter(bodies: &[Body]) -> Body {
    let barycenter = bodies[0];
    bodies.iter().skip(1).fold(barycenter, |barycenter, body| {
        merge_two_bodies(barycenter, *body)
    })
}

fn main() {
    let bodies = get_values();
    let barycenter = merge_all_bodies_iter(&bodies);
    println!(
        "barycenter: ({}, {}, {})\nmass: {}",
        barycenter.x, barycenter.y, barycenter.z, barycenter.mass
    );
}
